var filename = "test_1";

describe(filename, function() {
  beforeAll(function() {
    this.ctx = TestApi.createContext(filename);
  });

  afterAll(function() {
    TestApi.teardown(this.ctx);
  });

  it("Succeeding test", function() {
    expect(SomeTest.fetchCount()).toEqual(1);
  });

  xit("Disabled test", function() {
    SomeTest.fetchCount().toEqual(0);
  });

  it("Failing test", function() {
    expect(SomeTest.fetchCount()).toEqual(2);
  });
});
