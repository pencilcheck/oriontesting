/*
 * Copyright 2009-2018 C3 IoT, Inc. All Rights Reserved.
 * This material, including without limitation any software, is the confidential trade secret
 * and proprietary information of C3 IoT and its licensors. Reproduction, use and/or distribution
 * of this material in any form is strictly prohibited except as set forth in a written
 * license agreement with C3 IoT and/or its authorized distributors.
 * This product may be covered by one or more U.S. patents or pending patent applications.
 */

var filename = "test_homePage";

describe(filename, function () {
  function testSuite(browser) {
    beforeAll(function () {
      this.ctx = TestApi.createContext(filename);

      this.client = Skywalker.init({
        browserName: browser
      });

      this.domain = "http://localhost:8080/";

      this.page = UITestHomePage.make({
        domain: this.domain,
        skywalker: this.client
      });

      TestApi.setupVanityUrl(this.ctx);

      expect(this.client.sessionId).toBeDefined();
      expect(this.client.sessionInfo).toBeDefined();
    });

    describe('application', function () {
      beforeAll(function () {
        this.client.setAuthToken(this.page.username, this.page.password, this.domain);
        this.client.goto(this.domain);
        this.client.waitForAjax();
        this.client.waitForVisible(this.page.pageTitle, 180);
      });

      it('should redirect to the home page of the default application specified its c3ui.json', function () {
        this.client.assertEqual(this.client.url(), this.domain + this.page.url);
      });
      
      it('should render the default page of the smartbulb application', function () {
        this.client.assertEqual(this.page.rendered(), "true");
      });
    });

    afterAll(function () {
      this.client.end();
      TestApi.teardown(this.ctx);
    });
  }

  describe("testing", function () {
    var supported_browsers = TenantConfig.configJson("Skywalker_Supported_Browsers");
    if (supported_browsers) {
      supported_browsers.forEach(function (browser) {
        describe(browser, function () {
          testSuite(browser);
        });
      });
    }
  });
});
